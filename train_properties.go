package libtrainsim

import "strconv"

type VersionStruct struct {
	Major int64
	Minor int64
	Patch int64
}

func (b *VersionStruct) ToString() string{
	var retVal string

	retVal += strconv.FormatInt(b.Major,10) + "."
	retVal += strconv.FormatInt(b.Minor,10) + "."
	retVal += strconv.FormatInt(b.Patch,10)

	return retVal
}

type TrainProperties struct{
	TrainProperties040
}

type TrainProperties040 struct {
	Name    string   `json:"name"`
	Mass	float64   `json:"mass"`
	MaxVelocity	float64   `json:"maxVelocity"`
	MaxAcceleration	float64   `json:"maxAcceleration"`
	FormatVersion    string   `json:"formatVersion"`
	TrackDrag	float64   `json:"trackDrag"`
	AirDrag	float64   `json:"airDrag"`
	VelocityUnit    string   `json:"velocityUnit"`
}

func (b *TrainProperties040) Cast() TrainProperties{
	var retVal TrainProperties
	retVal.Name = b.Name
	retVal.Mass = b.Mass
	retVal.MaxVelocity = b.MaxVelocity
	retVal.MaxAcceleration = b.MaxAcceleration
	retVal.FormatVersion = b.FormatVersion
	retVal.TrackDrag = b.TrackDrag
	retVal.AirDrag = b.AirDrag
	retVal.VelocityUnit = b.VelocityUnit
	return retVal
}

func  (b *TrainProperties) IsValid() bool{
    tmp := b.TrainProperties040
    return tmp.IsValid()
}

func (b *TrainProperties040) IsValid() bool{
    if b.Name == "" || b.Mass == 0{
        return false
    }
    
    if b.MaxVelocity == 0 || b.MaxAcceleration == 0{
        return false
    }
    
    if ! (b.VelocityUnit == "ms" || b.VelocityUnit == "kmh" || b.VelocityUnit == ""){
        return false
    }
    
    return true
}
